﻿using System;
using System.Collections.Generic;

namespace Person
{
    /// <summary>
    /// Class the containing the entry point into the application
    /// </summary>
    public class Program
    {
        /// <summary>
        /// Static entry point in which the application is run from
        /// </summary>
        /// <param name="args">string array containing zero or more parametres that are supplied to the application</param>
        public static void Main(string[] args)
        {
            //Part 1: Construct three different Person objects
            //Using the default constructor
            var wifeCreature = new Person();
                wifeCreature.Address = "5029 Columbia Road Apt 102, Columbia, MD, 20144";
                wifeCreature.Age = 35;
                wifeCreature.FirstName = "Jane-Amy";
                wifeCreature.LastName = "Brousseau";
                wifeCreature.PhoneNumber = "757-945-8749";

            //Using the initialisation constructor
            var selfCreature = new 
                Person("5029 Columbia Road Apt 102, Columbia, MD, 20144", 33, "Adam", "Brousseau", "757-325-7886");

            //Using the copy constructor
            var babyCreature = new Person(selfCreature)
            {
                FirstName = "Adrina",
                Age = 5
            };

            //Part 2: Store the Person objects created above into a List
            var listOfPersons = new List<Person>
            {
                wifeCreature,selfCreature,babyCreature
            };

            //Part 3: Retrieve each Person object from the above list and print their information to the console
            listOfPersons.ForEach(currentPerson => Console.Write(currentPerson.ToString()));
        }
    }

    /// <summary>
    /// Class that represents information that a Person might be required to enter on a form 
    /// </summary>
    class Person
    {
        /// <summary>
        /// Default Constructor - Used to initalise all properties as the default value for their respective type
        /// </summary>
        public Person()
        {
            Address = default(string);
            Age = default(int);
            FirstName = default(string);
            LastName = default(string);
            PhoneNumber = default(string);
        }

        /// <summary>
        /// Initialisation Constructor - Used to initialise all the values required by the Person object
        /// </summary>
        /// <param name="address">string representing the address of the person</param>
        /// <param name="age">int representing the age of the person</param>
        /// <param name="firstName">string representing the person's first name</param>
        /// <param name="lastName">string representing the person's last name</param>
        /// <param name="phoneNumber">string representing the phone number of the person</param>
        public Person(string address, int age, string firstName, string lastName, string phoneNumber)
        {
            Address = address;
            Age = age;
            FirstName = firstName;
            LastName = lastName;
            PhoneNumber = phoneNumber;
        }

        /// <summary>
        /// Copy Constructor - Used to create a copy of an existing person
        /// </summary>
        /// <param name="existingPerson"></param>
        public Person(Person existingPerson)
        {
            Address = existingPerson.Address;
            Age = existingPerson.Age;
            FirstName = existingPerson.FirstName;
            LastName = existingPerson.LastName;
            PhoneNumber = existingPerson.PhoneNumber;
        }

        public string Address { get; set; }

        public int Age { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string PhoneNumber { get; set; }

        public override string ToString()
        {
            return string.Concat("This person has the following characteristics:", Environment.NewLine,
                "Name: ", FirstName, " ", LastName, Environment.NewLine, "Address: ", Address, Environment.NewLine,
                "Age: ", Age, Environment.NewLine, "Phone Number: ", PhoneNumber, Environment.NewLine, Environment.NewLine);
        }
    }
}
